﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using RabbitMqDemo.Common.Messages;
using System.Threading;
using RabbitMqDemo.Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Runtime.Serialization;

namespace RabbitMqDemo.ClientTwo
{
    public sealed class RabbitMqClientTwo : RabbitMqDemoClientBase
    {
        private readonly string _otherPeerQueueName;
        private readonly string _clientName;

        public RabbitMqClientTwo(string hostName, string clientName, string queueName, string otherPeerQueueName, bool defaultExchange) 
            : base(hostName, defaultExchange)
        {
            if (string.IsNullOrEmpty(ExchangeName)) throw new ArgumentNullException("ExchangeName");

            ListeningQueueName = queueName;
            _otherPeerQueueName = otherPeerQueueName;
            _clientName = clientName;
            DefaultExchange = defaultExchange;
        }

        public override void Start()
        {
           base.Start();

            if (!DefaultExchange)
            {
                Channel.ExchangeDeclare(ExchangeName, "fanout");
            }
            else
            {
                Channel.QueueDeclare(queue: ListeningQueueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                Channel.QueueDeclare(queue: _otherPeerQueueName,
                          durable: false,
                          exclusive: false,
                          autoDelete: false,
                          arguments: null);
            }
            Logger.Info($"{_clientName} was started.");

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += (model, ea) =>
            {
                Thread.Sleep(1 * 1000);

                var obj = Deserialize(ea.Body);
                switch (obj)
                {
                    case DemoMessage demoMessage:
                        Handle(demoMessage, _clientName);
                        break;
                    case string str:
                        Handle(str, _clientName);
                        break;
                    default: throw new SerializationException();
                }

                Channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };

            Channel.BasicConsume(queue: ListeningQueueName,
                                  autoAck: false,
                                  consumer: consumer);              
        }

        public override void Send(object message)
        {
            if (!DefaultExchange)
            {
                Channel.BasicPublish(exchange: ExchangeName,
                     routingKey: "",
                     basicProperties: null,
                     body: Serialize(message));
            }
            else
            {
                Channel.BasicPublish(exchange: "",
                    routingKey: _otherPeerQueueName,
                    basicProperties: null,
                    body: Serialize(message));
            }

            Logger.Info($"{_clientName} sent message {message} to {_otherPeerQueueName}");    
        }
    }
}
