﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using RabbitMqDemo.Common;
using RabbitMqDemo.Common.Messages;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMqDemo.ClientOne
{
    public sealed class RabbitMqClientOne: RabbitMqDemoClientBase
    {
        private readonly string _otherPeerQueueName;
        private readonly string _clientName;

        public RabbitMqClientOne(string hostName, string clientName, string queueName, string otherPeerQueueName, bool defaultExchange) 
            : base(hostName, defaultExchange)
        {
            _otherPeerQueueName = otherPeerQueueName;
            _clientName = clientName;
            ListeningQueueName = queueName;
            DefaultExchange = defaultExchange;
        }

        public override void Start()
        {
            base.Start();

            Channel.QueueDeclare(
                queue: ListeningQueueName, 
                durable: false, 
                exclusive: false, 
                autoDelete: false, 
                arguments: null);

            Channel.QueueDeclare(
                queue: _otherPeerQueueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);


            if (!DefaultExchange)
            {
                Channel.ExchangeDeclare(ExchangeName, "fanout");

                Channel.QueueBind(queue: ListeningQueueName,
                                  exchange: ExchangeName,
                                  routingKey: "");
            }


            Logger.Info($"{_clientName} was started.");

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += (model, ea) =>
            {
                var obj = Deserialize(ea.Body);
                switch (obj)
                {
                    case DemoMessage demoMessage:
                        Handle(demoMessage, _clientName);
                        break;
                    case string str:
                        Handle(str, _clientName);
                        break;
                    default: throw new SerializationException();
                }
            };


            Channel.BasicConsume(
                queue: ListeningQueueName,
                autoAck: true,
                consumer: consumer);
        }

        public override void Send(object message)
        {
            var body = Serialize(message);
            var properties = Channel.CreateBasicProperties();
            properties.Persistent = true;

            Channel.BasicPublish(exchange: "", routingKey: _otherPeerQueueName, basicProperties: properties, body: body);
            Logger.Info($"{_clientName}: DemoMessage sent to {_otherPeerQueueName}");
        }
    }
}
