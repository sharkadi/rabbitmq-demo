﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMqDemo.Common;
using RabbitMqDemo.Common.Messages;
using RabbitMqDemo.ClientOne;
using RabbitMqDemo.ClientTwo;

namespace RabbitMqDemo.CLI
{
    class Program
    {
       public static void Main(string[] args)
        {
            Console.WriteLine("Demo application started.");
            Console.WriteLine("Testing basic producer-consumer.");
            using (RabbitMqDemoClientBase clientOne = new RabbitMqClientOne("localhost", "RabbitMQCLientOne", "client-one", "client-two", true),
                                          clientTwo = new RabbitMqClientTwo("localhost", "RabbitMQCLientTwo", "client-two", "client-one", true))
            {
                clientOne.Start();
                clientTwo.Start();

                for (int i = 0; i < 10; i++)
                {
                    DemoMessage message = new DemoMessage($"This is a test message #{i}");
                    clientOne.Send(message);
                }

                Console.ReadKey();
            }

            Console.WriteLine();
            Console.WriteLine("Testing one producer to many consumers.");
            using (RabbitMqDemoClientBase clientOne = new RabbitMqClientOne("localhost", "RabbitMQCLientOne", "client-one", "client-two", false),
                                          clientTwo = new RabbitMqClientTwo("localhost", "RabbitMQCLientTwo", "client-two", "client-one", false),
                                          clientThree = new RabbitMqClientOne("localhost", "RabbitMQCLientThree", "client-three", "client-two", false))
            {
                clientOne.Start();
                clientTwo.Start();
                clientThree.Start();

                DemoMessage messageOne = new DemoMessage($"This is a test message #{new Random().Next(0, 1000)}");
                clientTwo.Send(messageOne);

                Console.ReadKey();
            }
        }
    }
}
