﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using NLog;
using RabbitMQ.Client;
using RabbitMqDemo.Common.Messages;

namespace RabbitMqDemo.Common
{
    /// <summary>
    /// Базовый класс для всех демонстрационных клиентов
    /// </summary>
    public abstract class RabbitMqDemoClientBase : IRabbitMqDemoClient, IDisposable
    {

        private readonly string _hostName;
        public string ListeningQueueName { get; protected set; }
        private IConnection _connection;
        protected IModel Channel { get; private set; }
        protected ILogger Logger { get; }
        protected bool DefaultExchange { get; set; }
        protected string ExchangeName { get; set; }

        public RabbitMqDemoClientBase(string hostName, bool defaultExchange)
        {
            _hostName = hostName;
            DefaultExchange = defaultExchange;
            Logger = LogManager.GetCurrentClassLogger();
            ExchangeName = "demoExchange";
        }

        public void Dispose()
        {
            Stop();
        }

        public virtual void Start()
        {
            Stop();

            var factory = new ConnectionFactory() {HostName = _hostName};
            _connection = factory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public virtual void Stop()
        {
            Channel?.Dispose();
            Channel = null;
            _connection?.Dispose();
            _connection = null;
        }

        public abstract void Send(object message);

        protected byte[] Serialize(object message)
        {
            using (var stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, message);
                stream.Flush();
                return stream.ToArray();
            }
        }

        protected TMessageType Deserialize<TMessageType>(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                return (TMessageType) new BinaryFormatter().Deserialize(stream);
            }
        }

        protected object Deserialize(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                return new BinaryFormatter().Deserialize(stream);
            }
        }

        protected void Handle(DemoMessage message, string clientName)
        {
            Logger.Info($"{clientName}: received DemoMessage {message}");
        }

        protected void Handle(string message, string clientName)
        {
            Logger.Info($"{clientName}: received string {message}");
        }
    }
}
