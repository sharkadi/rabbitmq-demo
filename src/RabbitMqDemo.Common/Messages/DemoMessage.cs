﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMqDemo.Common.Messages
{
    [Serializable]
    public sealed class DemoMessage
    {
        public DemoMessage(string text)
        {
            Text = text;
            Timestamp = DateTime.Now;
        }

        public string Text { get; set; }
        public DateTime Timestamp { get; set; }
        public override string ToString()
        {
            return $"Text=[\"{Text}\"], Timestamp={Timestamp}";
        }
    }
}
