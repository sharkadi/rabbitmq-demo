﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMqDemo.Common
{
    /// <summary>
    /// Интерфейс демонстрационной клиента
    /// </summary>
    public interface IRabbitMqDemoClient
    {
        /// <summary>
        /// Запуск клиента
        /// </summary>
        void Start();
        /// <summary>
        /// Остановка клиента
        /// </summary>
        void Stop();
        /// <summary>
        /// Отправка сообщения в очередь получателя
        /// </summary>
        /// <param name="message">Сообщение для отправки</param>
        void Send(object message);
    }
}
